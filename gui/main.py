from functools import partial
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from tkinter.messagebox import askyesno, showinfo

class MainGUI():
    """Класс, который взаимодействует с графической библиотекой"""
    def __init__(self, run_func) -> None:
        self.__root_window = tk.Tk()
        geometry = {
            "height": 270,
            "width":300,
            "px":200,
            "py":500
        }
        self.__window = self.__window_parametrs(
            geometry=self.__root_window.geometry,
            params=geometry
        )
        self.__root_window.title('PhotoSort')
        self.__root_window.resizable(False, False)
        
        self.__mod = tk.IntVar(value=2)
        self.__source_path = tk.StringVar()
        self.__destonation_path = tk.StringVar()
        self.__enabled = tk.BooleanVar(value=True)
        self.__run_func = run_func
        ico = tk.PhotoImage(file = "./gui/logo.png")
        self.__root_window.iconphoto(False, ico)

    def start(self):
        """
            Функция запускает основная функция,
            которая запускает графическую часть приложения
        """
        self.__preConfig()
        self.__addStaticElements()
        self.__root_window.mainloop()
    
    def __ask(self) -> bool:
        choise = askyesno(
            title="Директории не существует",
            message="Создать директорию?"
        )
        if(not choise or choise == None):
            return False
        return True
    
    def __preConfig(self):
        """
            Предконфигационная функция
        """
        for c in range(2):
            self.__root_window.columnconfigure(index=c, weight=1)
            
        for r in range(8):
            self.__root_window.rowconfigure(index=r, weight=1)
    
    def __addStaticElements(self):
        """
            Добавить статические элементы
        """
        lbl_frame = ttk.LabelFrame(
                self.__root_window,
                text="Конечное местоположение фотографий"
        )
        lbl_frame.grid(
            row=0,
            column=0,
            rowspan=2,
            columnspan=2,
            sticky=tk.EW,
            ipadx=0,
            ipady=0,
            padx=5,
            pady=0
        )
        
        for radio_btn in self.__add_radio_btn(lbl_frame, self.__mod):
            radio_btn.pack()
        self.__dir_lbl = ttk.Label(
            text="Укажите папку с фотографиями"
        )
        self.__dir_lbl.grid(
            row=3,
            column=0,
            columnspan=2
        )
        
        self.__clear_empty_dir = \
            ttk.Checkbutton(
                text="Удалить пустые папки",
                variable=self.__enabled
            )
        self.__clear_empty_dir.grid(
            row=2,
            column=0,
            columnspan=2,
            ipadx=0,
            ipady=0,
            padx=2,
            pady=2
        )
        
        self.__src_dir_path = ttk.Entry(textvariable=self.__source_path)
        self.__src_dir_path.grid(
            row=4,
            column=0,
            ipadx=2,
            ipady=3,
            padx=3,
            pady=0
        )
        self.__btn_path = ttk.Button(
            command=partial(self.__enter_path,self.__src_dir_path),
            text="Выбрать"
        )
        self.__btn_path.grid(row=4,column=1)
        
        self.__start_btn =\
            ttk.Button(
                text="Начать сортировку",
                command=self.__start_btn_fn
            )
        self.__start_btn.grid(row=7,
            column=0,
            columnspan=2
        )
    
    def __check_valid_input(self) -> bool:
        """
            Проверить правильность введённых данных
        """
        src_path = self.__source_path.get()
        dst_path = self.__destonation_path.get()
        mod = self.__mod.get()
        if(mod == 0 or mod == 1):
            if(src_path != '' and dst_path != ''):
                return True
        else:
            if(self.__source_path != ''):
                self.__destonation_path.set('')
                return True
        
        return False
        
    def __start_btn_fn(self):
        """
            Функция для кнопки начала сортировки
        """
        if(self.__check_valid_input()):
            cp = False
            if(self.__mod.get() == 0):
                cp = True
            self.__run_func(
                self.__source_path.get(),
                self.__destonation_path.get(),
                cp,
                self.__enabled.get(),
                self.__ask
            )
            showinfo(title="Уведомление",message="Работа окончена")
        
    def __enter_path(self, entry):
        """
            Функция вызова диалога ввыбора папки
        """
        entry.delete(0,tk.END)
        entry.insert(0, filedialog.askdirectory())

    def __add_radio_btn(self, tk_frame, var) -> list:
        """
            Функция, которая добавляет к фрейму radiobutton
        """
        main = [
            ttk.Radiobutton(
                tk_frame,
                text="Копировать все фотографии\nв новую папку",
                value=0,
                variable=var,
                command=self.__add_checkbtn
            ),
            ttk.Radiobutton(
                tk_frame,
                text="Переместить все фотографии\nв новую папку",
                value=1,
                variable=var,
                command=self.__add_checkbtn
            ),
            ttk.Radiobutton(
                tk_frame,
                text="Перегруппировать все фотографии\nв текущей папке",
                value=2,
                variable=var,
                command=self.__remove_checkbtn
            )
        ]
        return main

    def __add_checkbtn(self):
        """
            Функция добавляет Entry и Button
        """
        if(self.__window.height == 270):
            self.__window.height = 310
            
            self.__lbl_distance_path =\
                ttk.Label(text="Папка назначения")
            self.__lbl_distance_path.grid(
                row=5,
                column=0,
                columnspan=2
            )
            self.__dst_dir_path = ttk.Entry(
                textvariable=self.__destonation_path
            )
            self.__dst_dir_path.grid(
                row=6,
                column=0,
                ipadx=2,
                ipady=3,
                padx=3,
                pady=0
            )
            self.__btn_distance_path = ttk.Button(
                command=partial(self.__enter_path,self.__dst_dir_path),
                text="Выбрать"
            )
            self.__btn_distance_path.grid(
                row=6,
                column=1,
                pady=0
            )
            self.__root_window.update()
        
    def __remove_checkbtn(self):
        """
            Функция удаляет Entry и Button
        """
        if(self.__window.height == 310):
            self.__window.height = 270
            
            self.__lbl_distance_path.destroy()
            del self.__lbl_distance_path
            self.__dst_dir_path.destroy()
            del self.__dst_dir_path
            self.__btn_distance_path.destroy()
            del self.__btn_distance_path
            self.__root_window.update()
    
    class __window_parametrs():
        """
            Класс, который управляет размером окна
        """
        def __init__(self,geometry, params: dict):
            self.main_func = geometry
            for key, value in params.items():
                if(key == "height"):
                    self.__height = value
                if(key == "width"):
                    self.__width = value
                if(key == "px"):
                    self.__px = value
                if(key == "py"):
                    self.__py = value
            self.set_geometry()
        
        @property
        def height(self):
            return self.__height
        
        @height.setter
        def height(self, height):
            if(height > 10):
                self.__height=height
                self.set_geometry()
        
        @property
        def width(self):
            return self.__width
        
        @width.setter
        def width(self, width):
            if(width > 10):
                self.__width=width
                self.set_geometry()
        
        @property
        def py(self):
            return self.__py
        
        @py.setter
        def py(self, py):
            if(py > 10):
                self.__py=py
                self.set_geometry()
        
        @property
        def px(self):
            return self.__px
        
        @px.setter
        def px(self, px):
            if(px > 10):
                self.__px=px
                self.set_geometry()

        def set_geometry(self):
            geometry = "{}x{}+{}+{}".format(
                self.__width,
                self.__height,
                self.__py,
                self.__px
            )
            self.main_func(geometry)
