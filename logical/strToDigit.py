'''
    Библиотека с одной функцией
'''
import re

def to_int(main: str) -> int:
    """
        Функция для конвертации string в int
    """
    if(re.match(r'^\d*$', main)):
        len_main = len(main)
        integer_digit = 0
        for i in range(len_main):
            integer_digit += (pow(10, (len_main - i) - 1)) * int(main[i])
    elif(re.match(r'^-\d*$', main)):
        main = main[1:]
        len_main = len(main)
        integer_digit = 0
        for i in range(len_main):
            integer_digit += (pow(10, (len_main - i) - 1)) * int(main[i])
        integer_digit *= -1
    else:
        raise strToDigitEx("Is not a int number!")


    return integer_digit

class strToDigitEx(BaseException):
    def __init__(self, text):
        self.txt = text
