from logical.fileDict import FileDict
from shutil import copyfile, move
from os import mkdir
from os import path
from random import choice
from string import ascii_uppercase, digits, whitespace

class Move():
    def __init__(
        self,
        main_dict: FileDict,
        root_folder: str,
        sub_txt="PhotoSort",
        err_fn=None
    ):
        self.__dict = main_dict
        self.__root_folder = root_folder
        self.__sub_txt = sub_txt
        self.__err_fn = err_fn


    def __mkdir_if_ne(self, main_path, err_fn=None):
        """
            Функция проверяет существует ли папка
        """
        if(not path.isdir(main_path)):
            if(err_fn()):
                mkdir(main_path)
            else:
                return False

        return True

    def __gen_chars_for_filename(self, min=1, max=5):
        """
            Функция, которая генерирует название файла
        """
        population = ascii_uppercase + whitespace + digits
        charlist = [choice(population) for i in range(min, max)]
        return "".join(charlist)


    def __run(self, src_path: str,dest_folder_path: str, fn):
        """
            Функция перемещения файлов
        """
        this_file = path.basename(src_path)
        file_name, ext_file = path.splitext(this_file)
        while(path.isfile(path.join(dest_folder_path, this_file))):
            file_name = self.__gen_chars_for_filename(max=10)
            this_file = file_name + ext_file
        dest_path = path.join(dest_folder_path, this_file)
        fn(src_path, dest_path)
    
    def run(self, cp: bool = False):
        """
            Основная функция, если есть bool'евое значение cp,
            то выполнится функция копирования
        """
        if(not self.__mkdir_if_ne(self.__root_folder,self.__err_fn)):
            return

        for path_file, meta in self.__dict.get_iter_item():
            year_folder = path.join(
                self.__root_folder,
                self.__sub_txt + ' ' + str(meta.get_year())
            )
            self.__mkdir_if_ne(year_folder, lambda: True)
            dest_folder = path.join(year_folder, meta.get_month_ru())
            self.__mkdir_if_ne(dest_folder, lambda: True)
            if(not cp):
                self.__run(
                    src_path=path_file,
                    dest_folder_path=dest_folder,
                    fn=move
                )
            else:
                self.__run(
                    src_path=path_file,
                    dest_folder_path=dest_folder,
                    fn=copyfile
                )
