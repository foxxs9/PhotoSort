from os import walk
from os import path
from .getMetadataDate import MetaDate

class FileDict:
    """
        Данный класс составляет словарь,
        в котором будет записан путь до файла,
        а так же новое название
    """
    def __init__(self, folder_path):
        if(not path.isdir(folder_path)):
            raise Exception("Folder not exist.")

        self.path = folder_path
        self.__init_dict()

    def __init_dict(self):
        self.folder_and_path = dict()

        for dirpath, _, filenames in walk(self.path):
            for filename in filenames:
                filepath = path.join(dirpath, filename)
                try:
                    file_metadata = MetaDate(file_path=filepath)
                    self.folder_and_path.update({filepath: file_metadata})
                except Exception:
                    pass

    def get_iter_item(self):
        return self.folder_and_path.items()
