from exif import Image
from os.path import isfile
from datetime import datetime
from .strToDigit import to_int

class MetaDate:
    """
        Класс, который позволяет забирать дату из фотографии
    """
    def __init__(self, file_path):
        if(not isfile(file_path)):
            raise Exception("File not exist.")
        self.__path = file_path
        self.__read()
        self.__get_datetime()

    def __read(self):
        with open(self.__path, 'rb') as image_file:
            self.image_data = Image(image_file)

    def __get_datetime(self):
        if(not self.image_data.has_exif):
            raise Exception("Metadata not being read.")
        temp = self.image_data.datetime.split(' ')

        date = temp[0].split(':')
        time = temp[1].split(':')

        year = to_int(date[0])
        month=to_int(date[1])
        day=to_int(date[2])
        hour=to_int(time[0])
        minute=to_int(time[1])
        second=to_int(time[2])
        self._dt = datetime(
            year=year,
            month=month,
            day=day,
            hour=hour,
            minute=minute,
            second=second
        )

    def get_day(self):
        return self._dt.day

    def get_month_ru(self):
        months = (
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        )
        return months[self._dt.month]

    def get_year(self):
        return self._dt.year

    def get_dt(self):
        return self._dt
