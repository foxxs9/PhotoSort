#!/usr/bin/env python

from gui.main import MainGUI 
from logical.fileDict import FileDict
from logical.os_move import Move

def run_fn(src_path, dst_path, cp_mode, clr_dir, err_fn):
    if(dst_path == '' and (not cp_mode)):
        Move(
            main_dict=FileDict(src_path),
            root_folder=src_path,
            err_fn=err_fn
        ).run()
    elif(not cp_mode):
        Move(
            main_dict=FileDict(src_path),
            root_folder=dst_path,
            err_fn=err_fn
        ).run()
    else:
        Move(
            main_dict=FileDict(src_path),
            root_folder=dst_path,
            err_fn=err_fn
        ).run(cp_mode)
    

gui = MainGUI(run_fn)
gui.start()
