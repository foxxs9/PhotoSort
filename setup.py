from setuptools import setup

setup(name='photosort',
      version='1.0',
      description='This is program for sorting photos',
      url='https://codeberg.org/foxxs9/PhotoSort',
      author='foxxs9',
      author_email='none@example.com',
      license='None',
      packages=['gui','logical'],
      scripts=['bin/PhotoSort.py'],
      zip_safe=False)
